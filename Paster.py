# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 16:49:43 2020

@author: Ryzen
"""
import random
import cv2 
import numpy as np
from matplotlib import pyplot as plt
import os 

train_path ='new_test\mp'
training_names = os.listdir(train_path)



for i in range(len(training_names)):
    file_name = training_names[i] 
   
    src = r'C:\Users\Ryzen\Anaconda3\envs\PythonGPU\TestImages\new_test\mp\%s' % (file_name)
    img_hw = cv2.imread(r'C:/Users/Ryzen/Anaconda3/envs/PythonGPU/TestImages/new_test/hw1/hw3.png')
    print(src)
    img_mp = cv2.imread(src)
    gray_mp = cv2.cvtColor(img_mp, cv2.COLOR_BGR2GRAY)
   
    gray_hw = cv2.cvtColor(img_hw, cv2.COLOR_BGR2GRAY)
    
    img_mp_blur = cv2.GaussianBlur(gray_mp,(5,5),0)
    ret1,img_mp_bin = cv2.threshold(img_mp_blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU) #nema ekstrema na histogramu, sto je dobar znak
    img_hw_blur = cv2.GaussianBlur(gray_hw,(5,5),0)
    ret2,img_hw_bin = cv2.threshold(img_hw_blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU) #nema ekstrema na histogramu, sto je dobar znak
    threshold_level = 1
    coords = np.column_stack(np.where(img_mp_bin <  threshold_level))
    
    print(len(coords))
    
    done = False
    
    while not done:
        rand_height = random.randint(10, img_mp.shape[0]-img_hw.shape[0])
        rand_width = random.randint(10,img_mp.shape[1]-img_hw.shape[1])
        x_offset = rand_width
        y_offset = rand_height
        rows, cols = img_hw_bin.shape
        roi = img_mp_bin[y_offset:y_offset+img_hw_bin.shape[0], x_offset:x_offset+img_hw_bin.shape[1]]
        ret, mask = cv2.threshold(img_hw_bin, 200, 255, cv2.THRESH_BINARY_INV)
        
        mask_inv = cv2.bitwise_not(mask)
        count_mp = cv2.countNonZero(roi)
        count_hw = cv2.countNonZero(mask_inv)
        
        if  count_mp/count_hw > 1.1:
            done = True
    
    temp_mp_bin = cv2.bitwise_and(roi,roi,mask = mask_inv)
    img_hw_bin = cv2.bitwise_and(img_hw_bin,img_hw_bin,mask = mask)
    final_hw_bin = cv2.add(temp_mp_bin,img_hw_bin)
    img_mp_bin[y_offset:y_offset+img_hw_bin.shape[0], x_offset:x_offset+img_hw_bin.shape[1]] = final_hw_bin
    
    img1 = cv2.cvtColor(img_mp_bin,cv2.COLOR_GRAY2RGB)
   
    
    print(len(coords))  
    
        
    for i in range(len(coords)):
        k = coords[i][0]
        j = coords[i][1]
        img1[k,j] = img_mp[k,j]
    
    
    
    
    dst = r'C:\Users\Ryzen\Anaconda3\envs\PythonGPU\TestImages\new_test\combined1\%s' % (file_name)
    cv2.imwrite(dst,img1)







