# -*- coding: utf-8 -*-
"""
Created on Sun Mar  1 18:24:15 2020

@author: Ryzen
"""
from PIL import Image
import random
import cv2 
import numpy as np
from matplotlib import pyplot as plt
    # img_mp = Image.open(r"C:\Users\Ryzen\Desktop\TestImages\MP Text\mp1.png")
    
    # img_hw = Image.open(r"C:\Users\Ryzen\Desktop\TestImages\HW Text\hw1.png")
    

    
    
    
    #area = (rand_width,rand_height,rand_width+img_hw.width,rand_height+img_hw.height)
    #print(area)
    
    #img_mp.paste(img_hw, area)
    #img_mp.show()
    #img_mp.save(r"C:\Users\Ryzen\Desktop\TestImages\Combined Text\combined2.png")
    
    
    
    
    
    #th3 = cv2.adaptiveThreshold(img_mp,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
               # cv2.THRESH_BINARY,11,2)
    #Adaptive Gaussian se loze pokazao, noisy ispadaju slike
        
    # titles = ['Original Image', 'Global Thresholding (v = 127)',
    #             'Adaptive Gaussian Thresholding']
    # images = [img_mp,th1,th3]
    
    # for i in range(3):
    #     plt.subplot(2,2,i+1),plt.imshow(images[i],'gray')
    #     plt.title(titles[i])
    #     plt.xticks([]),plt.yticks([])
    # plt.show()

    
file_name = '00000201.tif' 
src = r'C:\Users\Ryzen\Desktop\TestImages\Combined Text\%s' % (file_name)

img_mp = cv2.imread(src,0)
img_hw = cv2.imread(r'C:\Users\Ryzen\Desktop\TestImages\HW Text\hw5.png',0)

    # ret1,th1 = cv2.threshold(img_mp,127,255,cv2.THRESH_BINARY) #dosta se dobro pokazao
    # ret2,th2 = cv2.threshold(img_mp,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    # Otsu's thresholding after Gaussian filtering

img_mp_blur = cv2.GaussianBlur(img_mp,(5,5),0)
ret1,img_mp_bin = cv2.threshold(img_mp_blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU) #nema ekstrema na histogramu, sto je dobar znak
img_hw_blur = cv2.GaussianBlur(img_hw,(5,5),0)
ret2,img_hw_bin = cv2.threshold(img_hw_blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU) #nema ekstrema na histogramu, sto je dobar znak



rand_height = random.randint(10, img_mp_bin.shape[0]-img_hw_bin.shape[0])
rand_width = random.randint(100,img_mp_bin.shape[1]-img_hw_bin.shape[1])
x_offset = rand_width
y_offset = rand_height

rows, cols = img_hw_bin.shape
roi = img_mp_bin[y_offset:y_offset+img_hw_bin.shape[0], x_offset:x_offset+img_hw_bin.shape[1]]
ret, mask = cv2.threshold(img_hw_bin, 200, 255, cv2.THRESH_BINARY_INV)
mask_inv = cv2.bitwise_not(mask)
temp_mp_bin = cv2.bitwise_and(roi,roi,mask = mask_inv)
img_hw_bin = cv2.bitwise_and(img_hw_bin,img_hw_bin,mask = mask)
final_hw_bin = cv2.add(temp_mp_bin,img_hw_bin)
img_mp_bin[y_offset:y_offset+img_hw_bin.shape[0], x_offset:x_offset+img_hw_bin.shape[1]] = final_hw_bin

# cv2.imshow('image1',img_mp_bin)
# cv2.waitKey()
# cv2.destroyAllWindows()




dst = r'C:\Users\Ryzen\Desktop\TestImages\train\hw\%s' % (file_name)
cv2.imwrite(dst,img_mp_bin)







